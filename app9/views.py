from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
def _login_(request):
    context ={}
    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('user_success'))

        else:
            context["error"]= "Please provide valid credentials."
            return render(request, 'login.html', context)
    else:
        return render(request, 'login.html', context)
def _logout_(request):
    if request.method =="POST":
        logout(request)
        return HttpResponseRedirect(reverse('user_login'))
def success(request): 
    context = {}
    context['user'] = request.user
    return render(request, 'success.html', context)


