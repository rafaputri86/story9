from django.contrib import admin 
from django.urls import path, include 
from . views import _login_, success, _logout_

urlpatterns =[
    path('', _login_, name= "user_login"),
    path('success/', success, name = "user_success"), 
    path('logout/', _logout_, name ="user_logout"),


]

